// index.js
const express = require('express')
const axios = require('axios')
const path = require('path')
const bodyParser = require('body-parser')
const multer = require('multer')
const upload = multer();
const helpers = require('./helpers')
const { Car } = require('./models')
const car = require('./models/car')
const {QueryTypes} = require('sequelize') 
PUBLIC_DIRECTORY = path.join(__dirname, 'public')
const app = express()

app.use(express.static(PUBLIC_DIRECTORY));
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, 'public/images')
    },
    filename: function(req, file, cb){
        console.log(file);
        cb(null, file.originalname)
    }
})

// API GET ALL CARS
app.get("/cars", (req, res) => {
  Car.findAll().then(cars=>{
    res.json(cars)
  })
})

// GET all cars
app.get('/', (req, res) => {
    const{filter, all, small, medium, large} = req.query
    axios
    .get("http://localhost:3001/cars")
    .then(cars=>{
      let dataTampil = cars.data;
      if(filter){
      dataTampil = cars.data.filter((car) => {
        const filterer = car.nama.toLowerCase().match(filter.toLowerCase()) || car.ukuran.toLowerCase().match(filter.toLowerCase())
        return filterer;
      })
    }
    if(all){
      dataTampil = cars.data.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(all.toLowerCase())
        return filterer;
      })
    }
    if(small){
      dataTampil = cars.data.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(small.toLowerCase())
        return filterer;
      })
    }
    if(medium){
      dataTampil = cars.data.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(medium.toLowerCase())
        return filterer;
      })
    }
    if(large){
      dataTampil = cars.data.filter((car) => {
        const filterer = car.ukuran.toLowerCase().match(large.toLowerCase())
        return filterer;
      })
    }

      res.render("index", {
        mobil : dataTampil, filter,
        page: "Dashboard",
        page1: "active",
        page2: "",
        user: "Radian Rasyid"
    })
    })
})

// Display to Create
app.get("/add/cars", (req, res) => {
    res.render("add", {
        action: "/cars",
        page: "Add Cars",
        page1: "",
        page2: "active",
        user: "Radian Rasyid"
    })
})

app.get("/list/:id", (req, res) => {
  const id = parseInt(req.params.id);
  axios
  .get(`http://localhost:3001/cars/${id}`)
  .then(cars=>{
    res.render("list", {
      cars: cars.data[0],
      page: "List Cars",
      page1: "",
      page2: "active",
      user: "Radian Rasyid"
    })
  })
})

// Create cars DATA
app.post("/api/cars", async (req, res) => {
  let upload = multer({ storage:storage, fileFilter: helpers.imageFilter }).single('image');
  console.log("REQ BODY",req.body);
  upload(req, res, function(err){
      // console.log(req.body);
      Car.create({
          nama: req.body.nama,
          sewa: req.body.sewa,
          ukuran: req.body.ukuran,
          image: req.file ? req.file.originalname : '',
      }).then(()=>{
        res.redirect("/")
      })
  })
})

// Update Cars Data
app.get("/cars/:id", (req, res) => {
  const id = parseInt(req.params.id);
  console.log(id);
  axios
  .get(`http://localhost:3001/cars/${id}`)
  .then(cars=>{
    // console.log(cars.data[0].nama);
    res.render("edit", {
      dataPilih : cars.data[0],
      page: "Edit Cars",
      page1: "",
      page2: "active",
      user: "Radian Rasyid"
    })
  })
})


app.post("/api/cars/:id", (req, res) => {
  let upload = multer({storage:storage, fileFilter:helpers.imageFilter}).single('image')
  const{id} = req.params;
  upload(req, res, function(err){
    if(err){
      throw err
    }
      const body = {
          nama: req.body.nama,
          sewa: req.body.sewa,
          ukuran: req.body.ukuran,
      }
      if(req.file){
        body.image = req.file.originalname;
      }
      Car.update(body,{
          where: {id:req.params.id}
        }).then(()=>{
          res.redirect("/")
        })
    })
  })

// delete cars Data
app.get("/delete/:id", (req, res) => {
    const{id} = req.params;
    axios.delete(`http://localhost:3001/cars/${id}`)
    .then(cars=>{
      res.redirect("/")
    })
})

app.listen(3000, ()=>{
    console.log(`http://localhost:3000`);
})
